
package arbolpostorden;

import java.util.ArrayList;
import javax.swing.JPanel;

/**
 *
 * @author ploks
 */
//CLASE QUE RECIVE LOS DATOS INSERTADOS POR EL USUARIO DESDE LA INTERFAZ GRAFICA, PARA ENVIARLOS A LA CLASE ARBOL 
public class Datos {

    Arbol miArbol = new Arbol();

    public Datos() {
    }

    public boolean insertar(Integer dato) {
        return (this.miArbol.agregar(dato));
    }

    public String borrar(Integer dato) {
        Integer x = this.miArbol.borrar(dato);
        if (x == null) {
            return ("El número ingresado no se encuentra en el arbol");
        }
        return ("Borrado el dato: " + x.toString());
    }
    //METOD PARA IMPRIMIR EL RECORRIDO POSTORDEN DEL GRAFO 
    public String posOrden() {
        ArrayList it = this.miArbol.postOrden();
        return (recorrido(it));
    }

    //MUESTRA LAS HOJAS DEL ARBOL 
    public String darHojas() {
        ArrayList it = this.miArbol.getHojas();
        return (recorrido(it));
    }

    //PRESENTA EL PADRE DEL NODO INGRESADO
    public String darPadre(Integer hijo) {
        if (this.miArbol.getRaiz().getDato() == (hijo)) {
            return ("La raiz no tiene padre");
        }
        Integer padre = this.miArbol.padre(hijo);
        if (padre == null) {
            return ("El número ingresado no existe: " + hijo.toString());
        }
        return ("El padre de: " + hijo + "\n es : " + padre.toString());
    }

    //BUSCA EL NODO INGRESADO
    public String esta(Integer dato) {
        boolean siEsta = this.miArbol.buscar(dato);
        String r = "El nodo con el número: " + dato.toString() + "\n";
        r += siEsta ? "Si se encuentra en el arbol" : "No se encuentra en el arbol";
        return (r);
    }
    
    //METODO PARA CONTROLAR QUE NO SE VUELVAN A INGRESAR NUMEROS Q YA HAN SIDO INGRESADOS
    public boolean repetido(Integer dato) {
        boolean datExis = this.miArbol.buscar(dato);
        return (datExis);
    }
    
    //METODO PARA RECORRER EL ARBOL
    private String recorrido(ArrayList it) {
        int i = 0;
        String r = " ";
        while (i < it.size()) {
            if (i==0) {
                r += it.get(i).toString() + " ";
            }
            else{
                r += ", "+ it.get(i).toString() + " ";
            }
            
            i++;
        }
        return (r);
    }
    
    public String CantidadNodos(){
        return this.miArbol.cantidadNodos();
    }
    
    public String CantidadHojas(){
        return this.miArbol.cantidadNodosHoja();
    }

    public String menorValor(){
        return this.miArbol.menorValor();
    }
    public String mayorValor(){
        return this.miArbol.mayorValor();
    }
    public String borrarMenor(){
        return this.miArbol.borrarMenor();
    }
    public String borrarMayor(){
        return this.miArbol.borrarMayor();
    }
   

    public void podarArbol() {
        this.miArbol.podar();
    }

    public JPanel getDibujo() {
        return this.miArbol.getdibujo();
    }
}
